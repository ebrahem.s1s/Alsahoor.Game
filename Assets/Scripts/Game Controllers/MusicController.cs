﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

	public static MusicController instance;

	private AudioSource audioSource;

	public AudioClip Arabic;

	public AudioClip English;
	void Start(){
		audioSource = GetComponent<AudioSource> ();

		if (GamePreferences.GetLanguageState () == 0) {

			audioSource.clip = English;
		} else {

			audioSource.clip = Arabic;
		}

	}
	void Awake () {
		MakeSingleton ();
	}

	public void ChangeAudioLang(){
		
		audioSource = GetComponent<AudioSource> ();


		if (GamePreferences.GetLanguageState () == 0) {

			audioSource.clip = English;
		} else {

			audioSource.clip = Arabic;
		}

	}
	void MakeSingleton() {
		if (instance != null) {
			Destroy(gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	public void PlayMusic(bool play) {
		if (play) {
			if (!audioSource.isPlaying) {
				audioSource.Play ();
			}
		} else {
			if (audioSource.isPlaying) {
				audioSource.Stop ();
			}
		}
	}

} // MusicController





























































