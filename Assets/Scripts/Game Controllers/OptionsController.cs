﻿using UnityEngine;
using System.Collections;

public class OptionsController : MonoBehaviour {

	[SerializeField]
	private GameObject easySign, mediumSign, hardSign;

	void Start() {

		SetInitialDifficultyInOptionsMenu ();
		GameManager.instance.paused ();
	}

	public void InitialDifficulty(string difficulty) {
		switch (difficulty) {
		case "easy":
			easySign.SetActive (true);
			mediumSign.SetActive (false);
			hardSign.SetActive (false);
			break;

		case "medium":
			easySign.SetActive (false);
			mediumSign.SetActive (true);
			hardSign.SetActive (false);
			break;

		case "hard":
			easySign.SetActive (false);
			mediumSign.SetActive (false);
			hardSign.SetActive (true);
			break;
		}
	}

	void SetInitialDifficultyInOptionsMenu() {
		if(GamePreferences.GetEasyDifficultyState() == 0) {
			InitialDifficulty("easy");
		} 
		
		if(GamePreferences.GetMediumDifficultyState() == 0) {
			InitialDifficulty("medium");
		} 
		
		if(GamePreferences.GetHardDifficultyState() == 0) {
			InitialDifficulty("hard");
		} 
	}

	public void EasyDifficulty() {

		GamePreferences.SetEasyDifficultyState (0);
		GamePreferences.SetMediumDifficultyState (1);
		GamePreferences.SetHardDifficultyState (1);

		easySign.SetActive (true);
		mediumSign.SetActive (false);
		hardSign.SetActive (false);
	}

	public void MediumDifficulty() {

		GamePreferences.SetEasyDifficultyState (1);
		GamePreferences.SetMediumDifficultyState (0);
		GamePreferences.SetHardDifficultyState (1);

		easySign.SetActive (false);
		mediumSign.SetActive (true);
		hardSign.SetActive (false);
	}

	public void HardDifficulty() {

		GamePreferences.SetEasyDifficultyState (1);
		GamePreferences.SetMediumDifficultyState (1);
		GamePreferences.SetHardDifficultyState (0);

		easySign.SetActive (false);
		mediumSign.SetActive (false);
		hardSign.SetActive (true);
	}

	public void GoBack() {

		if (GamePreferences.GetLanguageState () == 0) {
			Application.LoadLevel ("MainMenu");

		} else {
			Application.LoadLevel ("MainMenu_Arab");

		}


	}


	public void ChangeLanguage() {
		if (GamePreferences.GetLanguageState () == 0) {
			GamePreferences.SetLanguageState (1);


			Application.LoadLevel ("OptionsMenu_Arab");
	
			MusicController.instance.ChangeAudioLang ();

			MusicController.instance.PlayMusic (false);


			if (GamePreferences.GetMusicState () == 0) {
				if (MusicController.instance != null) {
					MusicController.instance.PlayMusic (true);
				}
			}


		} else {
			GamePreferences.SetLanguageState (0);

			Application.LoadLevel ("OptionsMenu");

			MusicController.instance.ChangeAudioLang ();

			MusicController.instance.PlayMusic (false);

			if (GamePreferences.GetMusicState () == 0) {
				if (MusicController.instance != null) {
					MusicController.instance.PlayMusic (true);
				}
			
			}
		}
	}

} // OptionsController
















































