﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighscoreController : MonoBehaviour {

	[SerializeField]
	private Text scoreText, coinText;

	void Start() {
		SetScoreForDifficulty ();

		GameManager.instance.paused ();

	}
	
	public void SetScore(int score, int coin) {
		scoreText.text = "" + score;
		coinText.text = "" + coin;
	}

	void SetScoreForDifficulty() {
		if(GamePreferences.GetEasyDifficultyState() == 0) {
			SetScore(GamePreferences.GetEasyDifficultyHighscore(), GamePreferences.GetEasyDifficultyCoinScore());
		} 
		
		if(GamePreferences.GetMediumDifficultyState() == 0) {
			SetScore(GamePreferences.GetMediumDifficultyHighscore(), GamePreferences.GetMediumDifficultyCoinScore());
		} 
		
		if(GamePreferences.GetHardDifficultyState() == 0) {
			SetScore(GamePreferences.GetHardDifficultyHighscore(), GamePreferences.GetHardDifficultyCoinScore());
		} 
	}

	public void GoBack() {
		
		if (GamePreferences.GetLanguageState () == 0) {
			Application.LoadLevel ("MainMenu");
		} else {
			Application.LoadLevel ("MainMenu_Arab");
		}
	
	}

} // HighscoreController


















































