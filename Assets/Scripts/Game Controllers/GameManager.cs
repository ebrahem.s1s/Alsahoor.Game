﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	int UntlNextAddInterest;
	[HideInInspector]
	public bool gameStartedFromMainMenu, gameRestartedAfterPlayerDied,gameOverFromManager = false;

	[HideInInspector]
	public int score, coinScore, lifeScore;

	void Awake () {
		MakeSingleton ();
	}

	void Start() {
		InitializeGame ();
	}

	void OnLevelWasLoaded() {
		if (Application.loadedLevelName == "Gameplay") {

		 if(gameStartedFromMainMenu) {
		

				GameplayController.instance.SetScore(0);
				GameplayController.instance.SetLifeScore(2);
				GameplayController.instance.SetCoinScore(0);

			}

		}
	}

	void MakeSingleton() {
		if (instance != null) {
			Destroy (gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}	
	public void paused(){
		if (UntlNextAddInterest >= 5) {
			UntlNextAddInterest = 0;
			GoogleMobileAdsDemoScript.instance.ShowInterstitial ();
		} else {
			UntlNextAddInterest++;
		}
	}
	void InitializeGame() {
		if (!PlayerPrefs.HasKey ("Game Initialized")) {
			GamePreferences.SetMusicState(1);

			GamePreferences.SetLanguageState (0);

			GamePreferences.SetEasyDifficultyState(1);
			GamePreferences.SetEasyDifficultyHighscore(0);
			GamePreferences.SetEasyDifficultyCoinScore(0);

			GamePreferences.SetMediumDifficultyState(0);
			GamePreferences.SetMediumDifficultyHighscore(0);
			GamePreferences.SetMediumDifficultyCoinScore(0);

			GamePreferences.SetHardDifficultyState(1);
			GamePreferences.SetHardDifficultyHighscore(0);
			GamePreferences.SetHardDifficultyCoinScore(0);

			PlayerPrefs.SetInt("Game Initialized", 0);
		}
	}

	public void CheckGameStatus(int score) {


		
		
		if (gameOverFromManager) {
			if (GamePreferences.GetEasyDifficultyState () == 0) {
				int highscore = GamePreferences.GetEasyDifficultyHighscore ();
				if (highscore < score)
					GamePreferences.SetEasyDifficultyHighscore (score);


			}

			if (GamePreferences.GetMediumDifficultyState () == 0) {
				int highscore = GamePreferences.GetMediumDifficultyHighscore ();
				if (highscore < score)
					GamePreferences.SetMediumDifficultyHighscore (score);


			}

			if (GamePreferences.GetHardDifficultyState () == 0) {
				int highscore = GamePreferences.GetHardDifficultyHighscore ();
				if (highscore < score)
					GamePreferences.SetHardDifficultyHighscore (score);


			}
			gameStartedFromMainMenu = false;

		}


	} 


} // GameManager

































































