﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : MonoBehaviour {
	

	[SerializeField]
	private Button musicButton;

	[SerializeField]
	private Sprite[] musicIcons;
	public AudioSource audioSource;
	public AudioClip audioClip;

	public void playClip(){
		audioSource.clip = audioClip;
		audioSource.Play();

	}

	void Update(){


	}
	void Start() {
		

		CheckIfMusicIsOnOrOff ();

	}

	void CheckIfMusicIsOnOrOff() {
		if (GamePreferences.GetMusicState () == 0) {
			if (MusicController.instance != null) {
				MusicController.instance.PlayMusic (true);
			}
			musicButton.image.sprite = musicIcons[0];
		} else {
			musicButton.image.sprite = musicIcons[1];
		}

	
	}

	public void PlayGame() {
		GameManager.instance.gameStartedFromMainMenu = true;
//		Application.LoadLevel ("Gameplay");
		if (GamePreferences.GetLanguageState () == 0) {
		SceneFader.instance.LoadLevel ("GamePlay");
		
		} else {
			SceneFader.instance.LoadLevel ("GamePlayArab");
		
		}


		MusicController.instance.PlayMusic (false);
	}

	public void HighScoreMenu() {
		if (GamePreferences.GetLanguageState () == 0) {
			Application.LoadLevel ("HighScore");
		} else {
			Application.LoadLevel ("HighScore_Arab");
		}

	}
	public void HowtoPlay() {
		if (GamePreferences.GetLanguageState () == 0) {
			Application.LoadLevel ("HowToPlay");
		} else {
			Application.LoadLevel ("HowToPlay_Arab");
		}

	}
	public void OptionsMenu() {
		if (GamePreferences.GetLanguageState () == 0) {
		Application.LoadLevel ("OptionsMenu");
		} else {
			Application.LoadLevel ("OptionsMenu_Arab");
		}
	}

	public void QuitGame() {
		Application.Quit ();
	}

	public void TurnMusicOnOrOff() {
		if (GamePreferences.GetMusicState () == 0) {
			GamePreferences.SetMusicState (1);
			if (MusicController.instance != null) {
				MusicController.instance.PlayMusic (false);
			}
			musicButton.image.sprite = musicIcons[1];
		} else {
			GamePreferences.SetMusicState (0);
			if (MusicController.instance != null) {
				MusicController.instance.PlayMusic (true);
			}
			musicButton.image.sprite = musicIcons[0];
		}
	}

} // MainMenuController














































