﻿using UnityEngine;
using System.Collections;

public class OtherButtonPlayController : MonoBehaviour {

	public static OtherButtonPlayController instance;

	private AudioSource audioSource;
	bool play = true;
	void Awake () {
		MakeSingleton ();
		audioSource = GetComponent<AudioSource> ();

	}

	void MakeSingleton() {
		if (instance != null) {
				Destroy(gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}


	public void PlayMusic() {

		StartCoroutine (PlayThenStop());

	}

	IEnumerator PlayThenStop(){
		play = true;

		if (play) {
			if (!audioSource.isPlaying) {
				audioSource.Play ();
			}
		}

		yield return new WaitForSeconds(0.4f);
		play = false;
		if(!play) {
			if (audioSource.isPlaying) {
				audioSource.Stop ();
			}
		}
		Destroy(gameObject);
	}

}
