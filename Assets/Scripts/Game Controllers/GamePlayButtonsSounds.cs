﻿using UnityEngine;
using System.Collections;

public class GamePlayButtonsSounds : MonoBehaviour {


	private AudioSource audioSource;
	bool play = true;
	void Awake () {

		audioSource = GetComponent<AudioSource> ();

	}


	public void PlayButton(){
		audioSource.Play ();

	}

	public void PlayMusic() {

		StartCoroutine (PlayThenStop());

	}

	IEnumerator PlayThenStop(){
		play = true;

		if (play) {
			if (!audioSource.isPlaying) {
				audioSource.Play ();
			}
		}

		yield return new WaitForSeconds(0.4f);
		play = false;
		if(!play) {
			if (audioSource.isPlaying) {
				audioSource.Stop ();
			}
		}
		Destroy(gameObject);
	}

}
