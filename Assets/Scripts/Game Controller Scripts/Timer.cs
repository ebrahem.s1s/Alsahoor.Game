﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
	public static Timer instance;

	private Slider slider;

	public static int scoreCount;


	public AudioSource EndGameAudio;
	public AudioSource BackgroundAudio;

	[SerializeField]
	private Text TimerText,ScoreText,ScoreGameOver,ScoreGameWon;
	[HideInInspector]
	public bool FoodNotMix = false;

	[SerializeField]
	private GameObject gameOver,PausePanel,GameWon,GameStarted;
	[HideInInspector]
	public static float air;

	private float airBurn = 1f;
	[HideInInspector]
	public float Score;
	int score;
	// conditions for drinks
	[HideInInspector]
	public bool IsMilkDrunk;
	[HideInInspector]
	public bool IsJuiceDrunk;

	//stomch stuff slide
	[HideInInspector]
	public float Stomchfloat = 0.1f;

	private static float stomachValueStarter = 50;
	[SerializeField]
	public Image MoonHalf;

	public float TimerOnGoing;
	//test
	Color tempColor;

	private Slider stomch;

	private Slider MilkDrink;
	private Slider JuiceDrink;

	float StomchfloatTwo;

	bool StomchThrowup = false;

	float milkTimeLimit = 10 ;
	float JuiceTimeLimit = 10;
	bool gameOverFromManager = false;
	void Start(){
		CheckGameStatus ();
		Time.timeScale = 1;
		IsMilkDrunk = false;
		IsJuiceDrunk = false;
		GoogleMobileAdsDemoScript.instance.HideBanner ();
		GameStarted.SetActive (true);
		Time.timeScale = 0;


	}
	public void ResetAir ()
	{	
		
		air = slider.maxValue;
	}

	// Use this for initialization
	void Awake()
	{		MakeInstance ();
		Score =0;
		GetRefrences();
		FoodNotMix = false;

		StomchfloatTwo = Stomchfloat;

		tempColor = MoonHalf.color;
		tempColor.a = 1;

	}

	public void CheckGameStatus() {


		if (GamePreferences.GetEasyDifficultyState () == 0) {
			air = 60;
			stomachValueStarter = 50;
		}

		if (GamePreferences.GetMediumDifficultyState () == 0){
			air = 45;
			stomachValueStarter = 70;
		}

		if (GamePreferences.GetHardDifficultyState () == 0) {
			air = 30;
			stomachValueStarter = 80;
		}

		slider.maxValue = air;
		TimerOnGoing = air;
	
	} 


	void FixedUpdate(){

		stomch.value = StomchfloatTwo;

		if (Stomchfloat >= StomchfloatTwo) {

			StomchfloatTwo += 0.1f;
	
		}
		if (StomchThrowup) {
			if (Stomchfloat <= StomchfloatTwo) {
				StomchfloatTwo -= 0.1f;
			} else {StomchThrowup = false;
			}
		}


		if (Stomchfloat >= stomch.maxValue) {
			

			GameWon.SetActive (true);
			GameOverCustom ();
			Time.timeScale = 0;
		}

		MoonHalf.color = tempColor;


	}
	// Update is called once per frame
	void Update () {
		score = (int) Score;
		ScoreText.text = "" + score;
		int currentTime = (int) air;

	
		TimerText.text = "" + currentTime;

		tempColor.a -= 1.0f/TimerOnGoing * Time.deltaTime;

		if(air >0)
		{ 
			air -= airBurn * Time.deltaTime;
			slider.value = air;

		
		}
		else
		{ 
			GameOverCustom ();

			gameOver.SetActive (true);
		
			Time.timeScale = 0;
			

		}


		// drinks conditions cooldowns
		if (IsMilkDrunk) {
			milkTimeLimit -= Time.deltaTime;
			MilkDrink.value = milkTimeLimit;
			if (milkTimeLimit <= 0) {
				IsMilkDrunk = false;
			}
			FoodButton.instance.MilkDrunkLowerStomch = 0.5f;
			FoodButton1.instance.MilkDrunkLowerStomch = 0.6f;
			FoodButton2.instance.MilkDrunkLowerStomch = 0.7f;
			FoodButton3.instance.MilkDrunkLowerStomch = 0.7f;
			FoodButton.instance.MilkDrunkNegativeScore = 0.9f;
			FoodButton1.instance.MilkDrunkNegativeScore = 0.9f;
			FoodButton2.instance.MilkDrunkNegativeScore = 0.9f;
			FoodButton3.instance.MilkDrunkNegativeScore = 0.9f;
		} else {
			FoodButton.instance.MilkDrunkLowerStomch = 1;
			FoodButton1.instance.MilkDrunkLowerStomch = 1;
			FoodButton2.instance.MilkDrunkLowerStomch = 1;
			FoodButton3.instance.MilkDrunkLowerStomch = 1;
			FoodButton.instance.MilkDrunkNegativeScore = 1;
			FoodButton1.instance.MilkDrunkNegativeScore = 1;
			FoodButton2.instance.MilkDrunkNegativeScore = 1;
			FoodButton3.instance.MilkDrunkNegativeScore = 1;
		}

		if (IsJuiceDrunk) {
			JuiceTimeLimit -= Time.deltaTime;
			JuiceDrink.value = JuiceTimeLimit;
			if (JuiceTimeLimit <= 0) {
				IsJuiceDrunk = false;	
			}
			airBurn = 1.3f;
			FoodButton3.instance.JuiceBonus = 1.2f;
			FoodButton2.instance.JuiceBonus = 1.2f;
			FoodButton1.instance.JuiceBonus = 1.2f;
			FoodButton.instance.JuiceBonus = 1.2f;
		} else { airBurn = 1;
			FoodButton3.instance.JuiceBonus = 1;
			FoodButton2.instance.JuiceBonus = 1;
			FoodButton1.instance.JuiceBonus = 1;
			FoodButton.instance.JuiceBonus = 1;
		}

	
	}

	void GetRefrences()
	{
		
		slider = GameObject.Find("Timer Slider Test").GetComponent<Slider>();

		slider.minValue = 0f;

		slider.value = slider.maxValue;

		stomch = GameObject.Find ("Stomch slide").GetComponent<Slider> ();
		stomch.minValue = 0.0f;
		stomch.maxValue = stomachValueStarter;
		stomch.value = 0;


		MilkDrink = GameObject.Find ("Milk Slider").GetComponent<Slider> ();
		MilkDrink.minValue = 0.0f;
		MilkDrink.maxValue = milkTimeLimit;
		MilkDrink.value = MilkDrink.maxValue;

		JuiceDrink = GameObject.Find ("Juice Slider").GetComponent<Slider> ();
		JuiceDrink.minValue = 0.0f;
		JuiceDrink.maxValue = JuiceTimeLimit;
		JuiceDrink.value = JuiceDrink.maxValue;
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;

	}
	public void GameisPaused() {

		GameManager.instance.paused ();

		BackgroundAudio.Pause ();
		PausePanel.SetActive (true);
		Time.timeScale = 0;
		GoogleMobileAdsDemoScript.instance.ShowBanner ();
	}
	public void ResumeGame(){
		BackgroundAudio.UnPause ();
		Time.timeScale = 1;
		GoogleMobileAdsDemoScript.instance.HideBanner ();
		GameStarted.SetActive (false);
		PausePanel.SetActive (false);

	}

	public void RestartGame(){
		
	
		Time.timeScale = 1;
		GoogleMobileAdsDemoScript.instance.HideBanner ();

		if (GamePreferences.GetLanguageState () == 0) {
			Application.LoadLevel ("GamePlay");
		} else {
			Application.LoadLevel ("GamePlayArab");
		}

	
	}



	public void BackToMainMenu(){
		Time.timeScale = 1;

		if (GamePreferences.GetLanguageState () == 0) {
			Application.LoadLevel ("MainMenu");
		} else {
			Application.LoadLevel ("MainMenu_Arab");
		}
		GameManager.instance.paused ();
	}
	public void GameOverCustom() {


		if (air > 0) {
			Score += air * 1.5f;;

		}

		score = (int)Score;
		ScoreGameWon.text = "" + score;
		ScoreGameOver.text = "" + score;
		ScoreText.text = "" + score;
			if (GamePreferences.GetEasyDifficultyState () == 0) {
				int highscore = GamePreferences.GetEasyDifficultyHighscore ();
				if (highscore < score)
					GamePreferences.SetEasyDifficultyHighscore (score);


			}

			if (GamePreferences.GetMediumDifficultyState () == 0) {
				int highscore = GamePreferences.GetMediumDifficultyHighscore ();
				if (highscore < score)
					GamePreferences.SetMediumDifficultyHighscore (score);


			}

			if (GamePreferences.GetHardDifficultyState () == 0) {
				int highscore = GamePreferences.GetHardDifficultyHighscore ();
				if (highscore < score)
					GamePreferences.SetHardDifficultyHighscore (score);


			}
		Time.timeScale = 1; 
		BackgroundAudio.Stop ();
		EndGameAudio.gameObject.SetActive (true);
		Time.timeScale = 0;
		GoogleMobileAdsDemoScript.instance.ShowBanner ();

		GameManager.instance.paused ();
	} 
}
