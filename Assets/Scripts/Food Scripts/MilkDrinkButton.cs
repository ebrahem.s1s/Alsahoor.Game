﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MilkDrinkButton : MonoBehaviour {

	public static MilkDrinkButton instance;

	[SerializeField]
	private Button MilkButton;

	void Start() {
		MakeInstance ();
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;

	}


	public void OnPointerDown(PointerEventData data) {
		Timer.instance.IsMilkDrunk = true;
		EndTheObject ();
	}
		
	public void EndTheObject(){
		Timer.instance.IsMilkDrunk = true;
		MilkButton.gameObject.SetActive (false);
		this.gameObject.SetActive (false);
	}
}
