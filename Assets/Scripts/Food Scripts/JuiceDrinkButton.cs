﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JuiceDrinkButton : MonoBehaviour {

	public static JuiceDrinkButton instance;

	[SerializeField]
	private Button JuiceOnButton;

	void Start() {
		MakeInstance ();
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;

	}



	public void OnPointerDown(PointerEventData data) {
		Timer.instance.IsJuiceDrunk = true;
		EndTheObject ();
	}

	public void EndTheObject(){
		Timer.instance.IsJuiceDrunk = true;
		this.gameObject.SetActive (false);
		JuiceOnButton.gameObject.SetActive (false);
	}
} //JuiceDrinkButton
