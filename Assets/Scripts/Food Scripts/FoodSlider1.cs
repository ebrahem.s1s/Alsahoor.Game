﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FoodSlider1 : MonoBehaviour{	
	public static FoodSlider1 instance;
	float angle;
	public float FoodTypeNumbers;
	float FirstValueNumType;

	void Awake(){
		MakeInstance();
		FirstValueNumType = FoodTypeNumbers;
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;

	}


	void FixedUpdate(){
		KeepCheckPrecent ();
	}

	void KeepCheckPrecent(){
		GetComponent<Image> ().fillAmount = angle;

		GetComponent<Image> ().color = Color.Lerp (Color.green, Color.red, angle);

	}
	public void MakePrecentage(){
		FoodTypeNumbers += FirstValueNumType * 2f;
		MakeFoodPercent ();

	}

	public void MakeFoodPercent()
	{	
		if (FoodTypeNumbers >= 330) {
			angle = 0;
			FoodTypeNumbers = FirstValueNumType;

			FoodButton1.instance.LowerFoodNum ();

		} else {
			// local pos is the mouse position.
			angle = FoodTypeNumbers / 360f;


		}




	}



}
