﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class FoodSoundButtonScript : MonoBehaviour {

	private AudioSource audioSource;

	bool play = true;

	void Awake () {

		audioSource = GetComponent<AudioSource> ();

	}


	public void PlayMusic() {

		StartCoroutine (PlayThenStop());

	}

	IEnumerator PlayThenStop(){

		if (play) {
			if (!audioSource.isPlaying) {
				audioSource.Play ();

			}
		}



		if(!play) {
			if (audioSource.isPlaying) {
				audioSource.Stop ();
			}
			play = false;
			yield return new WaitForSeconds(audioSource.clip.length);
			play = true;

		}
	}

}
