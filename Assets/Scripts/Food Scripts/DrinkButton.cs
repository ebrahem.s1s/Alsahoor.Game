﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DrinkButton : MonoBehaviour, IPointerDownHandler {

	public static DrinkButton instance;


	[SerializeField]
	private Button drinkMilkButton;
	[SerializeField]
	private Button drinkJuiceButton;

	[HideInInspector]
	public bool FoodNotMixDrink=false;


	void Awake() {
		MakeInstance ();
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;

	}
		

	public void OnPointerDown(PointerEventData data) {

		Timer.instance.IsJuiceDrunk = true;
		this.gameObject.SetActive (false);
	
	}


} // FoodButton
