﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FoodButton3 : MonoBehaviour, IPointerDownHandler {

	public static FoodButton3 instance;

	// Drinks Bonuses and negatives
	[HideInInspector]
	public float JuiceBonus = 1;
	[HideInInspector]
	public float MilkDrunkLowerStomch = 1;
	[HideInInspector]
	public float MilkDrunkNegativeScore = 1;



	[SerializeField]
	int FirstValueNum;
	int FoodNumber;
	[SerializeField]
	private Sprite[] foodIcon;
	[SerializeField]
	private Button foodButton;
	[HideInInspector]
	float StomchValue = 5f;
	[SerializeField]
	int FoodScore = 2;
	[SerializeField]
	float DcressVisibiltyValue;
	Color tempColor;
	Color tempColor2;

	[SerializeField]
	private Button FoodButton2;
	[SerializeField]
	private Sprite[] FoodIcon2;
	[SerializeField]
	int FirstValueNum2;
	int FoodNumber2;
	// Awake
	void Awake() {
		MakeInstance ();
		FoodNumber = 0;
		FoodNumber2 = 0;
		tempColor = foodButton.image.color;
		tempColor2= foodButton.image.color;

	}

	// instance
	void MakeInstance(){
		if (instance == null)
			instance = this;

	}

	// Check food
	void FadeFoodChecker(){
		if (tempColor.a >= 0.25f) {
			tempColor.a -= DcressVisibiltyValue;

		} 
	}
	// Fixed Update
	void FixedUpdate()
	{foodButton.image.color = tempColor2;
		if (tempColor.a < tempColor2.a) {
			tempColor2.a -= 0.01f;


		}
		if (tempColor.a <= 0.25f) {
			LowerFoodNum ();
		} 
	}

	// Lower Food value num
	public void LowerFoodNum() {


		Timer.instance.Score +=  2 * FoodScore * JuiceBonus * MilkDrunkNegativeScore;

		Timer.instance.Stomchfloat += StomchValue * MilkDrunkLowerStomch;
		FoodNumber++;
		FoodNumber2++;
		if (FoodNumber >= FirstValueNum) {
			EndTheObject ();
		} else {
			foodButton.image.sprite = foodIcon [FoodNumber];
			tempColor.a = 1f;
			tempColor2.a = 1f;
		}
		if (FoodNumber2 >= FirstValueNum2) {
			EndTheObject2 ();
		} else {
			FoodButton2.image.sprite = FoodIcon2 [FoodNumber2];
		}

	}

	public void EndTheObject2(){
		FoodButton2.gameObject.SetActive (false);

	}
	// end if no more vlaue
	public void EndTheObject(){
		tempColor.a = 1f;
		foodButton.gameObject.SetActive (false);
		this.gameObject.SetActive (false);

	}

	public void OnPointerDown(PointerEventData data) {
		FadeFoodChecker ();
	}

} // FoodButton
