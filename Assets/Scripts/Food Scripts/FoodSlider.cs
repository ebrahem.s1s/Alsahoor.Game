﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FoodSlider : MonoBehaviour{	
	public static FoodSlider instance;
	float angle;
	public float FoodTypeNumbers;
	float FirstValueNumType;

	void Awake(){
		MakeInstance();
		FirstValueNumType = FoodTypeNumbers;
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;

	}


	void FixedUpdate(){
		KeepCheckPrecent ();
	}

	void KeepCheckPrecent(){
		GetComponent<Image> ().fillAmount = angle;

		GetComponent<Image> ().color = Color.Lerp (Color.red, Color.green, angle);



	}
	public void MakePrecentage(){
		FoodTypeNumbers += FirstValueNumType * 2f;
		MakeFoodPercent ();

	}

	public void MakeFoodPercent()
	{	
		if (FoodTypeNumbers >= 330) {
			angle = 0;
			FoodTypeNumbers = FirstValueNumType;

			FoodButton.instance.LowerFoodNum ();

		} else {


			angle = FoodTypeNumbers / 360f;

		
		}




	}


	public void EndSliderObject (){

	}
}
